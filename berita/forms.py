from django import forms
from .models import Berita
import datetime

class BeritaForm(forms.ModelForm):
    class Meta:
        model = Berita
        fields = '__all__'

        widgets = {
            'date' : forms.DateInput(attrs={'class': 'form-control',
                                        'type' : 'date'}),
            'time' : forms.TimeInput(attrs={'class': 'form-control',
                                        'type' : 'time'}),

            'name' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Nama Anda'}),
            'judul' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Judul berita'}),
            'isi' : forms.Textarea(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Berita Anda'}),                            
            'url' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'URL (example: www.google.com)'}),
        }
