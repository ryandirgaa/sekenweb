from django.shortcuts import render, redirect
from .forms import BeritaForm
from .models import Berita

# Create your views here.
def isiberita(request):
    form = BeritaForm()

    if request.method == "POST":
        form = BeritaForm(request.POST)
        if form.is_valid():
            post = form.save(commit=True)
            post.save()
            return redirect('/berita/')
    else:
        form = BeritaForm()

    return render(request, 'isiberita.html', {'form' : form})

def home(request):
    daftar = Berita.objects.all()
    context = { 'obj' : daftar }
    return render(request, 'berita.html', context)

def detail(request, id):
    obj = Berita.objects.get(id=id)
    context = { "obj" : obj }
    return render(request, 'detail.html', context)
