from django.urls import path

from . import views

app_name = 'berita'

urlpatterns = [
    path('', views.home, name='home'),
    path('isiberita/', views.isiberita, name='isiberita'),
    path('<int:id>/', views.detail, name='detail'),
]