from django.db import models
import datetime

# Create your models here.
class Berita(models.Model):
    date = models.DateField(blank=True, null=True)
    time = models.TimeField(blank=True, null=True)
    name = models.CharField(max_length=100)
    judul = models.CharField(max_length=200)
    # gambar = models.ImageField(blank=True, null=True)
    isi = models.TextField()
    url = models.CharField(max_length=500)