from django.db import models
from django.contrib.auth.models import User

# Create your models here.

GENDER = [
    ('Male', 'Male'),
    ('Female', 'Female'),
]

class TambahanUserModel(User):
    image_url = models.URLField(max_length = 100, blank = True, null = True)
    gender = models.CharField(max_length=20, choices=GENDER, default="Male")
    job = models.CharField(max_length = 50, null = True)
    social_media = models.CharField(max_length = 50)