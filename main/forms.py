from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from main.models import TambahanUserModel
from django import forms

class BuatUserForm(UserCreationForm):
    password1 = forms.CharField(
            widget=forms.TextInput(attrs = {
            'placeholder': 'Password',
            'type' : 'password',
            }))
        
    password2 = forms.CharField(
            widget=forms.TextInput(attrs = {
            'placeholder': 'Re-enter Password',
            'type' : 'password',
            }))
        
    image_url = forms.URLField(
            required = False, widget=forms.URLInput(attrs = {
            'type' : 'url', 'placeholder' : 'Profile Picture URL',
            }))

    class Meta:
        model = TambahanUserModel
        fields = ['username', 'first_name', 'password1', 'password2', 'image_url', 'gender', 'social_media']

        widgets = {
            'username' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Username'}),
                                        
            'first_name' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Your name'}),

            'gender' : forms.Select(attrs = {'class' : 'form-control',
                                            'type' : 'option'}),
            
            'social_media' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Social Media. e.g. @abcd (ig)'})}
    