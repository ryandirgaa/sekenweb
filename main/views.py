from django.shortcuts import render, redirect
from main.forms import BuatUserForm
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from main.models import TambahanUserModel

def home(request):
    if request.user.is_authenticated:
        return render(request, 'main/home.html')
    else:
        return HttpResponseRedirect('/login/')

def registerUser(request):
    form = BuatUserForm()
    
    if request.method == 'POST':
        form = BuatUserForm(request.POST)
        
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(request, username = username, password = password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/login/')
                
        else:
            messages.info(request, 'Your data is invalid')

    response = {'form':form}
    return render(request, 'main/register.html', response)

def loginUser(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            messages.info(request, 'Username or password is incorrect')
    
    return render(request, 'main/login.html')

def logoutUser(request):
    logout(request)
    return HttpResponseRedirect('/')
